const os = require('os');
module.exports = {
  // https 相关配置
  https: {
    tls: {
      cert: `${__dirname}/cert/ouyunc.com.pem`,
      key: `${__dirname}/cert/ouyunc.com.key`
    },
    // websocket 使用的ip以及端口号
    host: '172.30.213.2',
    port: 4443
  },
  // mediasoup 相关配置
  mediasoup: {
    // mediasoup worker 的默认数量；默认和操作系统的cup数量保持相等
    numWorkers: Object.keys(os.cpus()).length,
    // worker 创建所需要的信息
    workerSettings: {
      logLevel: 'warn',
      logTags: [
        'info',
        'ice',
        'dtls',
        'rtp',
        'srtp',
        'rtcp',
        'rtx',
        'bwe',
        'score',
        'simulcast',
        'svc',
        'sctp'
      ],
      rtcMinPort: process.env.MEDIASOUP_MIN_PORT || 30000,
      rtcMaxPort: process.env.MEDIASOUP_MAX_PORT || 60000,
    },
    // 路由创建所需要的信息
    routerOptions: {
      mediaCodecs: [
        {
          kind: 'audio',
          mimeType: 'audio/opus',
          clockRate: 48000,
          channels: 2
        }, {
          kind: 'video',
          mimeType: 'video/VP8',
          clockRate: 90000,
          parameters:
          {
            'x-google-start-bitrate': 1000
          }
        }, {
          kind: 'video',
          mimeType: 'video/VP9',
          clockRate: 90000,
          parameters:
          {
            'profile-id': 2,
            'x-google-start-bitrate': 1000
          }
        }, {
          kind: 'video',
          mimeType: 'video/h264',
          clockRate: 90000,
          parameters:
          {
            'packetization-mode': 1,
            'profile-level-id': '4d0032',
            'level-asymmetry-allowed': 1,
            'x-google-start-bitrate': 1000
          }
        }, {
          kind: 'video',
          mimeType: 'video/h264',
          clockRate: 90000,
          parameters:
          {
            'packetization-mode': 1,
            'profile-level-id': '42e01f',
            'level-asymmetry-allowed': 1,
            'x-google-start-bitrate': 1000
          }
        }
      ]
    },
    // webrtc transport 传输通道相关信息
    webRtcTransportOptions:
    {
      // webrtc transport代表一个连接传输通道，ip代表transport的ip，端口号从workerSetting 中会选择一个合适的使用
      listenIps:
        [
          {
            ip: process.env.MEDIASOUP_LISTEN_IP || '172.30.213.2',
            announcedIp: process.env.MEDIASOUP_ANNOUNCED_IP || '106.15.42.106'
          }
        ],
      initialAvailableOutgoingBitrate: 1000000,
      minimumAvailableOutgoingBitrate: 600000,
      maxSctpMessageSize: 262144,
      // Additional options that are not part of WebRtcTransportOptions.
      maxIncomingBitrate: 1500000
    },
  }
}