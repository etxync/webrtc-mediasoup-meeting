module.exports = {
  // https 相关
  https: {
    tls: {
      cert: `${__dirname}/cert/ouyunc.com.pem`,
      key: `${__dirname}/cert/ouyunc.com.key`
    },
    // 信令使用的ip以及端口号
    host: '192.168.146.128',
    port: 4443
  },
}